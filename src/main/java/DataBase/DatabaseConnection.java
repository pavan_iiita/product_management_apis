package DataBase;

import Constants.DALConstants;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by pavan on 4/2/16.
 */
public class DatabaseConnection {
    public Connection getConnection() {
        Connection connection = null;
        int attempts = 2;
        while(attempts>0) {
            try {
                Class.forName(DALConstants.JDBC_DRIVER);
                connection = DriverManager.getConnection(DALConstants.DB_URL, DALConstants.USER, DALConstants.PASS);
                break;
            }
            catch (Exception e) {
                attempts--;
                e.printStackTrace();
            }
        }
        return connection;
    }
}
