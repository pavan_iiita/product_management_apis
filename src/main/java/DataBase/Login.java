package DataBase;

import Bean.Login.UserLogin;
import com.google.gson.JsonObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by pavan on 5/2/16.
 */

public class Login {
    public static JsonObject login( UserLogin pc) {
        DatabaseConnection db= new DatabaseConnection();
        JsonObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        try {
            String sql = "select * from user where email=? and pass=?";
            System.out.println(sql);
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, pc.getEmail());
            stmt.setString(2, pc.getPassword());
            //int tmp=stmt.executeUpdate(sql);
            ResultSet rs = stmt.executeQuery();
            boolean tmp = rs.last();
            System.out.println(rs.getRow());
            if(rs.getRow()==1){
                 res = new JsonObject();
                 res.addProperty("success", true);
                 res.addProperty("message","login successfully");
                 res.addProperty("auth_token",rs.getString("token"));
            }else {
                res = new JsonObject();
                res.addProperty("success", false);
                res.addProperty("message", "login unsuccessfull");
            }
            rs.close();
            stmt.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return res;
    }
}
