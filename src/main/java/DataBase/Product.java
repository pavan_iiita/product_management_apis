package DataBase;

import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static Check.AuthToken.chekAuthToken;

/**
 * Created by pavan on 5/2/16.
 */
public class Product {
    public static JsonObject delete(Integer pid, String auth_token) {
        DatabaseConnection db= new DatabaseConnection();
        JsonObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        if (chekAuthToken(auth_token)) {
            try {
                String sql = "DELETE FROM products where pid=?";
                System.out.println(sql);
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, pid);
                int tmp = stmt.executeUpdate();
                System.out.println(tmp);
                if (tmp == 1) {
                    res = new JsonObject();
                    res.addProperty("success", true);
                    res.addProperty("message", "delete product successfully");
                    JsonObject tmpObj = new JsonObject();
                    res.add("data", tmpObj);

                } else {
                    res = new JsonObject();
                    res.addProperty("success", false);
                    res.addProperty("message", "does not find a product with this id");
                    JsonObject tmpObj = new JsonObject();
                    res.add("data", tmpObj);
                }
                //rs.close();
                stmt.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }else {
            res = new JsonObject();
            res.addProperty("success", false);
            res.addProperty("message", " authentication fail");
        }
        return res;
    }
    public static JsonObject add(Bean.Product.Product proObj, String auth_token) {
        DatabaseConnection db= new DatabaseConnection();
        JsonObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        if (chekAuthToken(auth_token)) {
            try {
                String sql = "insert into products(pname,category,price,description)values (?, ?,?,?)";
                System.out.println(sql);
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, proObj.getProductName());
                stmt.setString(2, proObj.getProductCategory());
                stmt.setInt(3, proObj.getProductPrice());
                stmt.setString(4, proObj.getProductDescription());
                int tmp = stmt.executeUpdate();
                System.out.println(tmp);
                if (tmp == 1) {
                    res = new JsonObject();
                    res.addProperty("success", true);
                    res.addProperty("message", "product added successfully");
                    JsonObject tmpObj = new JsonObject();
                    res.add("data", tmpObj);

                } else {
                    res = new JsonObject();
                    res.addProperty("success", false);
                    res.addProperty("message", " adding product fail");
                    JsonObject tmpObj = new JsonObject();
                    res.add("data", tmpObj);
                }
                //rs.close();
                stmt.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }else{
            res = new JsonObject();
            res.addProperty("success", false);
            res.addProperty("message", " authentication fail");
        }
        return res;
    }
    public static JsonObject update(int id, Bean.Product.Product proObj,String auth_token) {
        DatabaseConnection db= new DatabaseConnection();
        JsonObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        if (chekAuthToken(auth_token)){
            try {
                String sql = "UPDATE products SET pname=? , category=? , price =? , description=? WHERE pid=?";
                System.out.println(sql);
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, proObj.getProductName());
                stmt.setString(2, proObj.getProductCategory());
                stmt.setInt(3, proObj.getProductPrice());
                stmt.setString(4, proObj.getProductDescription());
                stmt.setInt(5, id);
                //System.out.println(pc.getEmail());
                int tmp=stmt.executeUpdate();
                System.out.println(tmp);
                if(tmp==1){
                    res = new JsonObject();
                    res.addProperty("success", true);
                    res.addProperty("message","update product successfully");
                    JsonObject tmpObj= new JsonObject();
                    res.add("data",tmpObj);

                }else {
                    res = new JsonObject();
                    res.addProperty("success", false);
                    res.addProperty("message", " updating product fail");
                    JsonObject tmpObj= new JsonObject();
                    res.add("data", tmpObj);
                }
                stmt.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }else{
            res = new JsonObject();
            res.addProperty("success", false);
            res.addProperty("message", " authentication fail");
        }
        return res;
    }
    public static JSONObject search (String query, String auth_token) {
        DatabaseConnection db= new DatabaseConnection();
        JSONObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
            if(chekAuthToken(auth_token)){
                try {
                    String sql1 = "SELECT * FROM products WHERE pname LIKE ?";
                    System.out.println(sql1);
                    stmt = conn.prepareStatement(sql1);
                    stmt.setString(1, "%"+query+"%");
                    ResultSet rs1 = stmt.executeQuery();
                    JSONArray productArray = new JSONArray();
                    while(rs1.next()){
                        JSONObject  tmp=new JSONObject();
                        tmp.put("pname", rs1.getString("pname"));
                        tmp.put("category", rs1.getString("category"));
                        tmp.put("price", rs1.getString("price"));
                        tmp.put("description", rs1.getString("description"));
                        productArray.put(tmp);
                    }
                    res = new JSONObject();
                    res.put("success", true);
                    res.put("message", "successfully search");
                    res.put("data", productArray);
                    stmt.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    res = new JSONObject();
                    res.put("success", false);
                    res.put("message", " something went wrong");
                }
            }
            else {
                res = new JSONObject();
                res.put("success", false);
                res.put("message", " authentication fail");
            }
        return res;
    }
}
