package Endpoints.Login;

import Bean.Login.UserLogin;
import Bean.Login.UserLoginMapper;
import com.google.gson.JsonObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static DataBase.Login.login;

/**
 * Created by pavan on 4/2/16.
 */
public class LoginEndpoint extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //Run.start();
        System.out.println(request.getRequestURI().substring(request.getContextPath().length()));
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        UserLogin pc = UserLoginMapper.map(request);
        //System.out.println(pc);
        System.out.println(request.getParameter("id"));
        System.out.println(pc.email);
        JsonObject res=  login(pc);
        out.write(res.toString());
        //Run.end();
    }
}
