package Endpoints.Product.ModifyProduct;

import Bean.Login.UserLogin;
import Bean.Login.UserLoginMapper;
import Bean.Product.Product;
import Bean.Product.ProductMapper;
import Utils.JsonBodyReader;
import com.google.gson.JsonObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static DataBase.Login.login;

/**
 * Created by pavan on 4/2/16.
 */
public class ModifyProductEndpoint extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //Run.start();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Product proObj = ProductMapper.map(request);
        //System.out.println(pc);
        System.out.println(proObj.name);
        System.out.println(proObj.category);
        System.out.println(request.getParameter("id"));
        int id= Integer.parseInt(request.getParameter("id"));
        System.out.println(request.getParameter("auth_code"));
        String authCode = request.getParameter("auth_code");
        //login(pc);
        JsonObject res= DataBase.Product.update(id,proObj,authCode);
        out.write(res.toString());
        //out.write(Process.response(pc));
        //Run.end();
    }
}
