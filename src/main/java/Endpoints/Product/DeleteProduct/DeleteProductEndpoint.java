package Endpoints.Product.DeleteProduct;

import Bean.Login.UserLogin;
import Bean.Login.UserLoginMapper;
import DataBase.Product;
import com.google.gson.JsonObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static DataBase.Login.login;

/**
 * Created by pavan on 4/2/16.
 */
public class DeleteProductEndpoint extends HttpServlet{
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        //System.out.println(request.getRequestURI().substring(request.getContextPath().length()));
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        //UserLogin pc = UserLoginMapper.map(request);
        //System.out.println(pc);
        System.out.println(request.getParameter("id"));
        int id= Integer.parseInt(request.getParameter("id"));
        System.out.println(request.getParameter("auth_code"));
        String authCode = request.getParameter("auth_code");
        //System.out.println(pc.email);
        JsonObject res= Product.delete(id,authCode);
        out.write(res.toString());
        //Run.end();


    }
}
