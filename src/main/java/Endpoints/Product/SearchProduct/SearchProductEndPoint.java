package Endpoints.Product.SearchProduct;

import org.json.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by pavan on 5/2/16.
 */
public class SearchProductEndPoint extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //Run.start();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        //Product proObj = ProductMapper.map(request);
        //System.out.println(pc);
        //System.out.println(proObj.name);
        //System.out.println(proObj.category);
        System.out.println(request.getParameter("name"));
        String name = request.getParameter("name");
        System.out.println(request.getParameter("auth_code"));
        String authCode = request.getParameter("auth_code");
        JSONObject res= DataBase.Product.search(name ,authCode);
        out.write(res.toString());
    }
}
