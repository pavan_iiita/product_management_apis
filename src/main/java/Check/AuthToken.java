package Check;

import DataBase.DatabaseConnection;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by pavan on 5/2/16.
 */
public class AuthToken {
    public static boolean chekAuthToken(String auth_code){
        DatabaseConnection db= new DatabaseConnection();
        JSONObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        try{
            String sql = "SELECT * FROM user WHERE token=?";
            System.out.println(sql);
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, auth_code);
            ResultSet rs = stmt.executeQuery();
            //JSONArray productArray = new JSONArray();
            boolean tmp1 = rs.last();
            System.out.println(rs.getRow());
            if(rs.getRow()<1){
                return false;
            }else return true;
        }catch (Exception e1) {
            e1.printStackTrace();
            return false;
        }
    }
}
