package Bean.Login;

/**
 * Created by pavan on 4/2/16.
 */
public class UserLogin {
    public String email;
    public String password;

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getPassword() {
        return password;
    }

}
