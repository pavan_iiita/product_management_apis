package Bean.Product;

import Bean.Login.UserLogin;
import Utils.JsonBodyReader;
import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by pavan on 4/2/16.
 */
public class ProductMapper {
    public static Product map(HttpServletRequest request) {
        Gson gson = new Gson();
        String json = JsonBodyReader.read(request);
        Product pc = gson.fromJson(json, Product.class);
        return pc;
    }
}
