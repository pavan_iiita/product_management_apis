package Bean.Product;

/**
 * Created by pavan on 4/2/16.
 */
public class Product {
    public String name;
    public String category;
    public int price;
    public String description;

    public void setProductName(String name) {
        this.name = name;
    }
    public String getProductName() {
        return name;
    }

    public void setProductCategory(String category) {
        this.category = category;
    }
    public String getProductCategory() {
        return category;
    }

    public void setProductPrice(int price) {
        this.price = price;
    }
    public int getProductPrice() {
        return price;
    }
    public void setProductDescription(String desc) {
        this.description = desc;
    }
    public String getProductDescription() {
        return description;
    }
}
