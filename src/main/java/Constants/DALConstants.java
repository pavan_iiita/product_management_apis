package Constants;

/**
 * Created by pavan on 4/2/16.
 */
public class DALConstants {
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String host = "127.0.0.1";
    public static final String port = "3306";
    public static final String DB_URL = "jdbc:mysql://"+host+":"+port+"/wingify";
    public static final String USER = "root";
    public static final String PASS = "root";
}
