package Utils;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;

/**
 * Created by pavan on 4/2/16.
 */
public class JsonBodyReader {
    public static String read(HttpServletRequest request) {
        StringBuilder buffer = new StringBuilder();
        String data="";
        try {
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            data = buffer.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
